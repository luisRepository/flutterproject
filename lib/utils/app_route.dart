class AppRoutes {
  static const HOME = '/';
  static const CATEGORIES_ANIMES = '/categories-animes';
  static const CATEGORIES_ANIMES_DETAILS = '/categories-details';
  static const CATEGORIES_JOGOS = '/categories-jogos';
  static const CATEGORIES_JOGOS_DETAILS = '/jogos-details';

}
