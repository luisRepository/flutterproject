import 'package:flutter/material.dart';
import 'package:http/http.dart' as http;
import '/models/jogos_item_category.dart';
import '/models/category.dart';
import '../utils/constants.dart';
import 'dart:convert';

class JogosProvider with ChangeNotifier  {
  final String _baseUrl = '${Contants.BASE_API_URL}';
  List<Category> _items = [];
   List<JogosItemCategory> _itemsCategory = [];

  List<Category> get items => [..._items];
    List<JogosItemCategory> get itemsCategory => [..._itemsCategory];

  int get itemsCount {
    return _items.length;
  }

   int get itemsCategoryCount {
    return _itemsCategory.length;
  }

   Future loadCategories() async {
    var response = await http.get("$_baseUrl/jogosDb.json");
    List<dynamic> data = json.decode(response.body);
    _items.clear();
    if (data != null) {
      data.forEach((categoryData) {
        _items.add(Category(
            id: categoryData['id'],
            title: categoryData['title'],
            imageUrl: categoryData['imageUrl']));
      });
      notifyListeners();
    }
    return Future.value();
  }

    Future loadItemCategories() async {
    var response = await http.get("$_baseUrl/jogos.json");
    List<dynamic> data = json.decode(response.body);
    _itemsCategory.clear();
    if (data != null) {
      data.forEach((categoryData) {
        _itemsCategory.add(JogosItemCategory(
            id: categoryData['id'],
            title: categoryData['title'],
            categories: categoryData['categories'],
            imageUrl: categoryData['imageUrl'],
            description: categoryData['Description'],
            rank: categoryData['rank']
            ));
      });
      notifyListeners();
    }
    return Future.value();
  }

}