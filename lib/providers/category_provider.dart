import 'package:flutter/material.dart';
import 'package:http/http.dart' as http;
import 'package:projetosx/models/item_category.dart';
import '../models/category.dart';
import '../utils/constants.dart';
import 'dart:convert';

class CategoryProvider with ChangeNotifier {
  final String _baseUrl = '${Contants.BASE_API_URL}';
  List<Category> _items = [];
  List<ItemCategory> _itemsCategory = [];

  List<Category> get items => [..._items];
  List<ItemCategory> get itemsCategory => [..._itemsCategory];

  int get itemsCount {
    return _items.length;
  }

  int get itemsCategoryCount {
    return _itemsCategory.length;
  }

  Future loadCategories() async {
    var response = await http.get("$_baseUrl/animesDb.json");
    List<dynamic> data = json.decode(response.body);
    _items.clear();
    if (data != null) {
      data.forEach((categoryData) {
        _items.add(Category(
            id: categoryData['id'],
            title: categoryData['title'],
            imageUrl: categoryData['imageUrl']));
      });
      notifyListeners();
    }
    return Future.value();
  }

  Future loadItemCategories() async {
    var response = await http.get("$_baseUrl/animes.json");
    List<dynamic> data = json.decode(response.body);
    _itemsCategory.clear();
    if (data != null) {
      data.forEach((categoryData) {
        _itemsCategory.add(ItemCategory(
            id: categoryData['id'],
            title: categoryData['title'],
            categories: categoryData['categories'],
            imageUrl: categoryData['imageUrl'],
            temporada: categoryData['temporada'],
            duration: categoryData['duration'],
            episodios: categoryData['episodios'],
            description: categoryData['Description']));
      });
      notifyListeners();
    }
    return Future.value();
  }
}
