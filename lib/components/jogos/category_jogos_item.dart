import 'package:flutter/material.dart';
import '/utils/app_route.dart';
import '/models/jogos_item_category.dart';

class CategoryJogosItem extends StatelessWidget {
  final JogosItemCategory jogosItemCategory;

  CategoryJogosItem(this.jogosItemCategory);

  void _selectAnime(BuildContext context) {
     Navigator.of(context).pushNamed(AppRoutes.CATEGORIES_JOGOS_DETAILS,arguments: jogosItemCategory);
  }
  @override
  Widget build(BuildContext context) {
    return InkWell(
      onTap: () => _selectAnime(context),
      child: Card(
          shape:
              RoundedRectangleBorder(borderRadius: BorderRadius.circular(15)),
          elevation: 4,
          margin: EdgeInsets.all(10),
          child: Column(children: [
            Stack(
              children: [
                ClipRRect(
                  borderRadius: BorderRadius.only(
                    topLeft: Radius.circular(15),
                    topRight: Radius.circular(15),
                  ),
                  child: Image.network(
                    jogosItemCategory.imageUrl,
                    height: 200,
                    width: double.infinity,
                    fit: BoxFit.cover,
                  ),
                ),
                Positioned(
                  right: 10,
                  bottom: 20,
                  child: Container(
                    width: 300,
                    color: Colors.black54,
                    padding: EdgeInsets.symmetric(vertical: 5, horizontal: 20),
                    child: Text(
                      jogosItemCategory.title,
                      style: TextStyle(fontSize: 26, color: Colors.white),
                      softWrap: true,
                      overflow: TextOverflow.fade,
                    ),
                  ),
                )
              ],
            ),
            Padding(
              padding: EdgeInsets.all(20),
              child: Row(
                mainAxisAlignment: MainAxisAlignment.spaceAround,
                children: [
                  Row(
                    children: [
                      Text(
                        'Classificação: ${jogosItemCategory.rank}-',
                        style: TextStyle(color: Colors.black, fontSize: 24),
                      ),          
                      Icon(Icons.star),                     
                      SizedBox(
                        width: 6,
                      ),
                    ],
                  ),
                ],
              ),
            ),
          ])),
    );
  }
}
