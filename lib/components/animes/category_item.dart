import 'package:flutter/material.dart';
import 'package:projetosx/utils/app_route.dart';
import '../../views/animes/categories_animes_view.dart';
import '../../models/category.dart';

class CategoryItem extends StatelessWidget {
  final Category category;

  CategoryItem(this.category);

  void _selectCategory(BuildContext context){
     Navigator.of(context).pushNamed(AppRoutes.CATEGORIES_ANIMES, arguments: category);
  }

  @override
  Widget build(BuildContext context) {
    return InkWell(
      onTap: () => _selectCategory(context),
      splashColor: Theme.of(context).primaryColor,
      borderRadius: BorderRadius.circular(15),
      child: Stack(
        children:[ Container(
          width: 200,
          height: 100,
            padding: EdgeInsets.all(15),
            child: Text(""),
            decoration: BoxDecoration(
              image: DecorationImage(
                  image: NetworkImage(category.imageUrl), fit: BoxFit.cover),
              borderRadius: BorderRadius.circular(15),
            )),
            Positioned(
               bottom: 20,
               right: 10,
               child: Container(
                 width: 150,
                 color: Colors.black26,
                 padding: EdgeInsets.symmetric(vertical: 5, horizontal: 20),
                 child: Text(
                   category.title,
                   style: TextStyle(
                     fontSize: 26,
                     color: Colors.white,
                   ),
                   textAlign: TextAlign.end,
                   softWrap: true,
                   overflow: TextOverflow.fade,
                 ),
               )
            )
        ]),
    );
  }
}
