import 'package:flutter/material.dart';
import 'package:projetosx/models/item_category.dart';
import 'package:projetosx/utils/app_route.dart';

class CategoryAnimeItem extends StatelessWidget {
  final ItemCategory itemCategory;

  CategoryAnimeItem(this.itemCategory);

  void _selectAnime(BuildContext context){
    Navigator.of(context).pushNamed(AppRoutes.CATEGORIES_ANIMES_DETAILS,arguments: itemCategory);
  }

  @override
  Widget build(BuildContext context) {
    return InkWell(
      onTap: () => _selectAnime(context),
      child: Card(
        shape: RoundedRectangleBorder(borderRadius: BorderRadius.circular(15)),
        elevation: 4,
        margin: EdgeInsets.all(10),
        child: Column(
          children: [
            Stack(
              children: [
                ClipRRect(
                  borderRadius: BorderRadius.only(
                    topLeft: Radius.circular(15),
                    topRight: Radius.circular(15),
                  ),
                  child: Image.network(
                    itemCategory.imageUrl,
                    height: 200,
                    width: double.infinity,
                    fit: BoxFit.cover,
                  ),
                ),
                Positioned(
                  right: 10,
                  bottom: 20,
                  child: Container(
                    width: 300,
                    color: Colors.black54,
                    padding: EdgeInsets.symmetric(vertical: 5, horizontal: 20),
                    child: Text(
                      itemCategory.title,
                      style: TextStyle(fontSize: 26, color: Colors.white),
                      softWrap: true,
                      overflow: TextOverflow.fade,
                    ),
                  ),
                )
              ],
            ),
            Padding(
              padding: EdgeInsets.all(20),
              child: Row(
                mainAxisAlignment: MainAxisAlignment.spaceAround,
                children: [
                  Row(
                    children: [
                      Icon(Icons.schedule),
                      SizedBox(
                        width: 6,
                      ),
                      Text(
                        '${itemCategory.duration}',
                        style: TextStyle(color: Colors.black),
                      )
                    ],
                  ),
                  Row(
                    children: <Widget>[
                      Icon(Icons.all_inclusive),
                      SizedBox(width: 6),
                      Text("${itemCategory.temporada} Temporadas",
                          style: TextStyle(color: Colors.black)),
                    ],
                  ),
                  Row(
                    children: <Widget>[
                      Icon(Icons.star_outlined),
                      SizedBox(width: 6),
                      Text('${itemCategory.episodios} Episódios',
                          style: TextStyle(color: Colors.black)),
                    ],
                  )
                ],
              ),
            )
          ],
        ),
      ),
    );
  }
}
