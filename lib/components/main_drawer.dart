import 'package:flutter/material.dart';
import '../utils/app_route.dart';

class MainDrawer extends StatelessWidget {
  Widget _createItem(
      IconData icon, String label, BuildContext context, String link) {
    return ListTile(
      leading: Icon(
        icon,
        size: 26,
      ),
      title: Text(
        label,
        style: TextStyle(
          color: Colors.black45,
            fontFamily: 'RobotoCondensed',
            fontSize: 24,
            fontWeight: FontWeight.bold),
      ),
      onTap: () => Navigator.of(context).pushReplacementNamed(link),
    );
  }

  @override
  Widget build(BuildContext context) {
    return Drawer(
      child: Column(
        children: [
          Container(
            height: 80,
            width: double.infinity,
            padding: EdgeInsets.all(15),
            color: Theme.of(context).primaryColor,
            alignment: Alignment.bottomLeft,
            child: Text(
              'Menu',
              style: TextStyle(
                  fontWeight: FontWeight.w900,
                  fontSize: 30,
                  color: Theme.of(context).accentColor),
            ),
          ),
          SizedBox(height: 20),
          _createItem(Icons.home, 'Home', context, AppRoutes.HOME)
        ],
      ),
    );
  }
}
