import 'package:flutter/material.dart';
import 'package:projetosx/providers/jogos_provider.dart';
import 'package:projetosx/views/jogos/jogos_details_view.dart';
import 'package:provider/provider.dart';
import 'utils/app_route.dart';
import 'views/animes/categories_details.dart';
import 'views/jogos/category_jogos_view.dart';
import 'views/tabs_view.dart';
import './providers/category_provider.dart';
import 'views/animes/categories_animes_view.dart';

void main() {
  runApp(MyApp());
}

class MyApp extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    return MultiProvider(
      providers: [
        ChangeNotifierProvider(
          create: (_) => new CategoryProvider(),
        ),
        ChangeNotifierProvider(
          create: (_) => new JogosProvider(),
        )
      ],
      child: MaterialApp(
          title: "Projeto Sx",
          theme: ThemeData(
              primarySwatch: Colors.indigo,
              accentColor: Colors.white,
              fontFamily: 'Raleway',
              canvasColor: Color.fromRGBO(179, 217, 255, 1),
              textTheme: ThemeData.dark().textTheme.copyWith(
                  headline6:
                      TextStyle(fontSize: 20, fontFamily: 'RobotoCondesed'))),
          routes: {
            AppRoutes.HOME: (ctx) => TabsView(),
            AppRoutes.CATEGORIES_ANIMES: (ctx) => CategoriesAnimesView(),
            AppRoutes.CATEGORIES_ANIMES_DETAILS: (ctx) => CategoriesDetails(),
            AppRoutes.CATEGORIES_JOGOS: (ctx) => CategoryJogosView(),
            AppRoutes.CATEGORIES_JOGOS_DETAILS: (ctx) => JogosDetailsView(),
          }),
    );
  }
}
