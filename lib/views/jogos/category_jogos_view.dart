import 'package:flutter/material.dart';
import '/components/jogos/category_jogos_item.dart';
import '/models/category.dart';
import '/providers/jogos_provider.dart';
import 'package:provider/provider.dart';

class CategoryJogosView extends StatefulWidget {
  @override
  _CategoryJogosViewState createState() => _CategoryJogosViewState();
}

class _CategoryJogosViewState extends State<CategoryJogosView> {
  bool _isLoading = true;

  @override
  void initState() {
    super.initState();
    Provider.of<JogosProvider>(context, listen: false).loadItemCategories().then((_) {
      setState(() {
        _isLoading = false;
      });
    });
  }

  @override
  Widget build(BuildContext context) {
    final category = ModalRoute.of(context)?.settings.arguments as Category;
    final categoryData = Provider.of<JogosProvider>(context);

    var categoryItems = categoryData.itemsCategory.where((element) {
      return element.categories.contains(category.id);
    }).toList();
    
    return Scaffold(
      appBar: AppBar(
        title: Text(category.title),
      ),
      body: _isLoading
          ? Center(
              child: CircularProgressIndicator(color: Colors.indigo),
            )
          : ListView.builder(
              itemCount: categoryItems.length,
              itemBuilder: (ctx, index) {
                return CategoryJogosItem(categoryItems[index]);
              },
            ),
    );
  }
}
