import 'package:flutter/material.dart';
import 'package:projetosx/components/jogos/category_jogos.dart';
import '/providers/jogos_provider.dart';
import 'package:provider/provider.dart';

class JogosView extends StatefulWidget {
  @override
  _JogosViewState createState() => _JogosViewState();
}

class _JogosViewState extends State<JogosView> {
  bool _isLoading = true;

  @override
  void initState() {
    super.initState();
    Provider.of<JogosProvider>(context, listen: false)
        .loadCategories()
        .then((_) {
      setState(() {
        _isLoading = false;
      });
    });
  }

  @override
  Widget build(BuildContext context) {
    var categoryData = Provider.of<JogosProvider>(context);
    var categoryItem = categoryData.items;
    return _isLoading
        ? Center(
            child: CircularProgressIndicator(),
          )
        : GridView(
            padding: EdgeInsets.all(25),
            gridDelegate: SliverGridDelegateWithMaxCrossAxisExtent(
                maxCrossAxisExtent: 200,
                childAspectRatio: 3 / 2,
                crossAxisSpacing: 20,
                mainAxisSpacing: 20),
            children: categoryItem.map((cat) {
              return CategoryJogos(cat);
            }).toList(),
          );
  }
}
