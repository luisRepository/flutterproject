import 'package:flutter/material.dart';
import '/models/jogos_item_category.dart';

class JogosDetailsView extends StatelessWidget {

      Widget _createSectionTitle(BuildContext context, String title) {
    return Container(
      margin: EdgeInsets.symmetric(vertical: 10),
      child: Text(
        title,
        style: TextStyle(fontWeight: FontWeight.bold, fontSize: 26, color: Colors.black),
      ),
    );
  }

    Widget _createSectionContainer(Widget child) {
    return Container(
      width: 400,
      padding: EdgeInsets.all(10),
      margin: EdgeInsets.all(10),
      decoration: BoxDecoration(
          color: Colors.white,
          border: Border.all(color: Colors.grey),
          borderRadius: BorderRadius.circular(10)),
      child: child,
    );
  }

  @override
  Widget build(BuildContext context) {
  final jogos = ModalRoute.of(context)?.settings.arguments as JogosItemCategory;
  return Scaffold(
      appBar: AppBar(
        title: Text(jogos.title),
      ),
      body: SingleChildScrollView(
        child: Column(
          children: [
            Container(
              padding: EdgeInsets.symmetric(vertical: 5, horizontal: 0),
              height: 200,
              width: double.infinity,
              child: Image.network(
                jogos.imageUrl,
                fit: BoxFit.cover,
              ),
            ),
            _createSectionTitle(context, 'Descrição'),
            _createSectionContainer(Card(
              child: Padding(
                padding: EdgeInsets.symmetric(vertical: 5, horizontal: 10),
                child: Text(
                  jogos.description,
                  textAlign: TextAlign.left,
                  style: TextStyle(
                    fontSize: 18,
                    fontWeight: FontWeight.bold,
                    color: Colors.black
                  ),
                ),
              ),
            ))
          ],
        ),
      ),
    );
  }
}