import 'package:flutter/material.dart';
import 'animes/categories_view.dart';
import 'home_view.dart';
import '../components/main_drawer.dart';
import 'jogos/jogos_view.dart';

class TabsView extends StatefulWidget {
  @override
  _TabsViewState createState() => _TabsViewState();
}

class _TabsViewState extends State<TabsView> {
  int _selectedScreenIndex = 0;
  final List<Map<String, dynamic>> _screens = [
    {'title': 'Home', 'screen': HomeView()},
    {'title': 'Categories', 'screen': CategoriesView()},
    {'title': 'Jogos', 'screen': JogosView()}
  ];

  _selectScreen(int index) {
    setState(() {
      _selectedScreenIndex = index;
    });
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        title: Text(_screens[_selectedScreenIndex]['title']),
        centerTitle: true,
      ),
      drawer: MainDrawer(),
      body: _screens[_selectedScreenIndex]['screen'],
      bottomNavigationBar: BottomNavigationBar(
        onTap: _selectScreen,
        backgroundColor: Theme.of(context).primaryColor,
        unselectedItemColor: Colors.white,
        selectedItemColor: Colors.yellow,
        currentIndex: _selectedScreenIndex,
        items: [
            BottomNavigationBarItem(
            icon: Icon(Icons.home),
            label: 'Home'
          ),
          BottomNavigationBarItem(
            icon: Icon(Icons.category),
            label: 'Categorias'
          ),
          BottomNavigationBarItem(
            icon: Icon(Icons.gamepad),
            label: 'Jogos'
          )
        ],
      ),
    );
  }
}
