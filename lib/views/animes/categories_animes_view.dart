import 'package:flutter/material.dart';
import '/providers/category_provider.dart';
import 'package:provider/provider.dart';
import '../../components/animes/category_animes_item.dart';
import '/models/category.dart';

class CategoriesAnimesView extends StatefulWidget {
  @override
  _CategoriesAnimesViewState createState() => _CategoriesAnimesViewState();
}

class _CategoriesAnimesViewState extends State<CategoriesAnimesView> {
  bool _isLoading = true;

  @override
  void initState() {
    super.initState();
    Provider.of<CategoryProvider>(context, listen: false)
        .loadItemCategories()
        .then((response) {
      setState(() {
        _isLoading = false;
      });
    });
  }

  @override
  Widget build(BuildContext context) {
    final category = ModalRoute.of(context)?.settings.arguments as Category;
    final categoryData = Provider.of<CategoryProvider>(context);

    var categoryItems = categoryData.itemsCategory.where((element) {
      return element.categories.contains(category.id);
    }).toList();

    return Scaffold(
      appBar: AppBar(
        title: Text(category.title),
      ),
      body: _isLoading
          ? Center(
              child: CircularProgressIndicator(color: Colors.indigo),
            )
          : ListView.builder(
              itemCount: categoryItems.length,
              itemBuilder: (ctx, index) {
                return CategoryAnimeItem(categoryItems[index]);
              },
            ),
    );
  }
}
