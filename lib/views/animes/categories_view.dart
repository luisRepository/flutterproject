import 'package:flutter/material.dart';
import 'package:projetosx/components/animes/category_item.dart';
import 'package:projetosx/providers/category_provider.dart';
import 'package:provider/provider.dart';

class CategoriesView extends StatefulWidget {
  @override
  _CategoriesViewState createState() => _CategoriesViewState();
}

class _CategoriesViewState extends State<CategoriesView> {
  bool _isLoading = true;
  
  @override
  void initState() {
    super.initState();
    Provider.of<CategoryProvider>(context, listen: false)
        .loadCategories()
        .then((_) {
      setState(() {
        _isLoading = false;
      });
    });
  }

  @override
  Widget build(BuildContext context) {
    final categoryData = Provider.of<CategoryProvider>(context);
    final category = categoryData.items;
    return _isLoading
        ? Center(
            child: CircularProgressIndicator(),
          )
        : GridView(
            padding: EdgeInsets.all(25),
            gridDelegate: SliverGridDelegateWithMaxCrossAxisExtent(
                maxCrossAxisExtent: 200,
                childAspectRatio: 3 / 2,
                crossAxisSpacing: 20,
                mainAxisSpacing: 20),
            children: category.map((cat) {
              return CategoryItem(cat);
            }).toList(),
          );
  }
}
