import 'package:flutter/material.dart';

class ItemCategory with ChangeNotifier  {
  String id;
  String title;
  String categories;
  String imageUrl;
  String temporada;
  String duration;
  String episodios;
  String description;

  ItemCategory(
      {required this.id,
      required this.title,
      required this.categories,
      required this.imageUrl,
      required this.temporada,
      required this.duration,
      required this.episodios,
      required this.description});
}
