import 'package:flutter/material.dart';

class JogosItemCategory with ChangeNotifier  {
  String id;
  String categories;
  String title;
  String description;
  String imageUrl;
  String rank;

  JogosItemCategory({
    required this.id,
    required this.categories,
    required this.title,
    required this.description,
    required this.imageUrl,
    required this.rank,
  });
}
