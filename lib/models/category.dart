import 'package:flutter/cupertino.dart';

class Category with ChangeNotifier {
  String id;
  String title;
  String imageUrl;

  Category({required this.id, required this.title, required this.imageUrl});
  
}
